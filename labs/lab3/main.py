#!/usr/bin/python
# Based heavily on multi_server.py example by Drew Gislason

from sense import PiSenseHat
from flask import Flask, request
import string
import json
import sys
import base64
import binascii
import xml.etree.ElementTree as ET
import observation_pb2

PORT = 5000
content_json      = 'application/json'
content_xml       = 'application/xml'
content_protobufs = 'application/protobuf'

# Content-Types
type_JSON     = 0
type_XML      = 1
type_PROTOBUF = 2

obs_dict = {'pressure' : 0.0,'temp' : 0.0 , 'humid' : 0.0}

sensors = PiSenseHat()

def updateObservations():
    global obs_dict
    obs_dict['temp'] = sensors.getTemperature();
    obs_dict['humid'] = sensors.getHumidity();
    obs_dict['pressure'] = sensors.getPressure();
    return

# create the global Flask objects
app = Flask(__name__)

# ======================= API Routes ======================
@app.route("/my/api/observation", methods=['GET'])
def observationRouteHandler():
    global obs_dict
    rsp_data = ""

    updateObservations()

    # assume json
    content_type = type_JSON

    if 'Content-Type' in request.headers:
        print "Content-Type: " + request.headers['Content-Type']
    else:
        print "No Content-Type specified" + "\n"

    if request.headers['Content-Type'] == content_xml:
      content_type = type_XML
    elif request.headers['Content-Type'] == content_protobufs:
      content_type = type_PROTOBUF
    elif request.headers['Content-Type'] == content_json:
      content_type = type_JSON

    # return dict as json (easiest option)
    if content_type == type_JSON:
        rsp_data = json.dumps(obs_dict) + '\n'

    # return the observation in XML format
    elif content_type == type_XML:
        rsp_data = '<observation>\n' \
        '<pressure>' + str(obs_dict['pressure']) + '</pressure>\n' + \
        '<temp>'     + str(obs_dict['temp'])     + '</temp>\n' + \
        '<humid>'    + str(obs_dict['humid'])    + '</humid>\n' + \
        '</observation>\n\n'

    # return the number in Google Protobuf format
    elif content_type == type_PROTOBUF:
        myvar = observation_pb2.Observation()
        myvar.stationName = "timm-pi"
        myvar.stationId = 102;
        myvar.baroPres = obs_dict['pressure'];
        myvar.Temp = obs_dict['temp'];
        mystr = myvar.SerializeToString()
        rsp_data = base64.b64encode(mystr) + '\n'

    else:
        rsp_data = 'Output format not specified' + '\n'
  
    return rsp_data, 200

# ============================== Main ====================================

if __name__ == "__main__":

  app.debug = True
  app.run(host='0.0.0.0', port=PORT, debug=True, threaded=True)

