# IoT210 Lab 3 #
## Implementing Restful APIs with Flask including json, XML, and protobufs ##

This lab implements a realistic IoT scenario: an API that returns 
current weather observations as if at a station in 3 returned data variants: json, XML, 
and Google protobufs. 

**Note that the main.py has to be run as sudo because of the PiSenseHat**

The json is easily generated with Python's native methods. Because the XML tree was 
small, it was generated manually. The files necessary to generate the protobuf were
produced using the Google protoc tool
```
protoc observation.proto --python_out=.
```

/my/api/observation
a GET only API that returns the observations in json, XML, or protobuf
format

Here is example output using curl's H flag to send the Content-Type tag:

```
timm@timm-iot110:~/iot210/labs/lab3 $ curl http://127.0.0.1:5000/my/api/observation -H "Content-Type: application/json"
{"pressure": 1015.071044921875, "humid": 33.303279876708984, "temp": 27.922401428222656}


timm@timm-iot110:~/iot210/labs/lab3 $ curl http://127.0.0.1:5000/my/api/observation -H "Content-Type: application/xml"

<observation>
<pressure>1015.03417969</pressure>
<temp>28.0102481842</temp>
<humid>33.497631073</humid>
</observation>

timm@timm-iot110:~/iot210/labs/lab3 $ curl http://127.0.0.1:5000/my/api/observation -H "Content-Type: application/protobuf"
Cgd0aW1tLXBpEGYdDMd9RCXi7OBB

```


