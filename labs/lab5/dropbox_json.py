#!/usr/bin/python
# =============================================================================
#        File : dropbox_json.py
# Description : persist password data in json on dropbox
# Heavily uses example code provided by instructor except uses json/python 
# dict for storage of data
# =============================================================================

import base64
import getpass
import sys
import string
import os
import json

# installed libraries
from Crypto.Hash import SHA256
import dropbox

DEBUG = 1
STATUS_OK = 1

# See: http://md5decrypt.net/en/Sha256/
# //www.dropbox.com/developers/apps/info/appkey

init_dict = {'hello': 'b5f972cb767017d24cc726e4d018711d0be874fa40ef2513c97a2abb526d42dc' }

auth_dict = {'hello': 'b5f972cb767017d24cc726e4d018711d0be874fa40ef2513c97a2abb526d42dc' }

TOKEN    = 'oTy4wXKxtq0AAAAAAABMA4dJmv7lRIt2KdBCOpzmdI8PHCHliwk3qLQmwsEJjEAY'
FILENAME = 'passwords.json'
TMPFILE  = 'temp.json'
SALT     = '$n3@_'

# login using secure token
def dropbox_login(token):
  dbx = dropbox.Dropbox(token)
  try:
    info = dbx.users_get_current_account()
    return dbx
  except:
    print "not a valid token"
    return None

# get the whole file from dropbox into memory as a string
def dropbox_get_jsonfile(dbx, json_filename):
  try:
    os.remove(TMPFILE)
  except:
    print "no tmp file"
  try:
    dbx.files_download_to_file(TMPFILE, json_filename)
    auth_dict = json.load(TMPFILE)
  except:
    print "could not download file " + json_filename
    return None

# write a dropbox file, given entire file contents.
# Returns size of file, or 0 if failed
def dropbox_put_jsonfile(dbx, json_filename):
    global auth_dict

    try:
        dbx.files_delete(json_filename)
    except:
        print "no dropbox file to delete " + json_filename

    try:
        with open(json_filename, 'rw+') as f:
            json.dump(auth_dict, f)
        f.close()
        dbx.files_upload(json_filename)
    except:
        print "exception opening json file " + json_filename
    return

# if username matches, return password sha256
def iot_find_password_sha(uname):
  global auth_dict

  # empty file or user, not found
  if uname == None:
    return None

  if uname in auth_dict:
      return auth_dict[uname]
  else:
      return None

# create a user with hashed password
# returns status
def iot_create_account():
  global auth_dict

  # don't allow creating the same name twice
  uname = raw_input("Enter username to create: ")
  if iot_find_password_sha(uname):
    return STATUS_DUPLICATE

  # get username and password in one line
  pwd   = SALT + getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()
  auth_dict[uname] = sha1 

  return STATUS_OK

# login a user with hashed password
def iot_login_account(uname):

  if DEBUG: print "uname " + str(uname)

  # get sha from password
  pwd   = SALT + getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()
  if DEBUG: print "sha1: " + str(sha1)

  # login if the sha matches
  sha2  = iot_find_password_sha(uname)
  if DEBUG: print "sha2: " + str(sha2)
  if sha2 == sha1:
    return True
  return False

# main function
def main():
  global auth_dict
  print "dropbox_json based on dropbox_sha256 v1.0"

  # login to dropbox
  dbx = dropbox_login(TOKEN)
  if dbx:
    print "\nlogged into dropbox"
    dropbox_get_jsonfile(dbx, FILENAME)
    with open(FILENAME, 'rw+') as f:
      json.dump(auth_dict, f)
      dropbox_put_jsonfile(dbx, FILENAME)
  else:
    print "\nfailed to log in to dropbox!"
    exit(1)

  while True:
    # get username
    uname = raw_input("\nEnter name to login (leave empty for new user): ")
    if uname == 'exit':
      break

    # create an account
    elif uname == '':
      status = iot_create_account()
      if status == STATUS_OK:
        if dropbox_put_jsonfile(dbx, FILENAME) == 0:
          print "Something went wrong"
        else:
          print "Account created"
      elif status == STATUS_DUPLICATE:
        print "Account already exists"

    # login to an account
    else:
      if iot_login_account(uname):
        print "Welcome " + uname + "! Logged in"
      else:
        print "Login failed!!"

if __name__ == "__main__":
  if len(TOKEN) <= len("{token}"):
    print "\nLooks like you didn't add your token. Go generate one for"
    print "your app API at: https://www.dropbox.com/developers/apps\n"
  main()
