# IoT210 Lab 2 
## Implementing Restful APIs with Flask

**Note that the main.py has to be run as sudo because of the PiSenseHat** 

This lab implements 3 APIs:

```
/my/api/topics?
```
a GET only API that returns the topics that are known by the server
Default topics include the sensor values of **temp, pressure, and humid 
(humidity) measured by the PiSenseHat**.

```
/my/api/value?
```
a GET and POST API that returns the numeric value of the topics. The
GET returns the value and the POST causes the Pi to read the sensor
hat for topics that the sensor hat provides

```
/my/api/prime/nn
```
a GET only API that returns whether a number provided is prime or not

Here are a few examples

```
timm@Mitymite2:~$ curl http://timm-iot110/my/api/topic?temp
temp
timm@Mitymite2:~$ curl http://timm-iot110/my/api/topic?humid
humid
timm@Mitymite2:~$ curl http://timm-iot110/my/api/topic?pressure
pressure
timm@Mitymite2:~$ curl http://timm-iot110/my/api/topic?rain

timm@Mitymite2:~$ curl http://timm-iot110/my/api/value?temp
0.0
timm@Mitymite2:~$ curl -X POST http://timm-iot110/my/api/value?temp
26.5519752502
timm@Mitymite2:~$ curl http://timm-iot110/my/api/value?humid
0.0
timm@Mitymite2:~$ curl -X POST http://timm-iot110/my/api/value?humid
41.2415771484
timm@Mitymite2:~$ curl http://timm-iot110/my/api/value?pressure
0.0
timm@Mitymite2:~$ curl -X POST http://timm-iot110/my/api/value?pressure
0timm@Mitymite2:~$ curl -X POST http://timm-iot110/my/api/value?pressure
1019.74023438
timm@Mitymite2:~$ curl http://timm-iot110/my/api/value?pressure
1019.74023438
timm@Mitymite2:~$ curl http://timm-iot110/my/api/prime/67
67 is prime
timm@Mitymite2:~$ curl http://timm-iot110/my/api/prime/68
68 is not prime

```


