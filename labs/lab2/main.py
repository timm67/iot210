#!/usr/bin/python

import time
import socket
from sense import PiSenseHat
from flask import Flask, request

SERVER_PORT = 5000

topics = ['pressure','temp','humid']

topic_dict = {'pressure' : 0.0,'temp' : 0.0 , 'humid' : 0.0}

sensors = PiSenseHat()

# create the global Flask objects
app = Flask(__name__)

# ======================= API Routes ======================
# get list of GET'table topics
@app.route("/my/api/topic", methods=['GET', 'PUT', 'POST', 'DELETE'])
def topicRouteHandler():
    global topics
    retList = ""
    if request.method == 'GET':
        for arg in request.args:
            print "arg: " + arg
            if arg in topics:
                retList = retList + arg
                print "retlist: " + retList
        return retList + "\n", 200
    elif request.method == 'POST':
        # add a topic
        for arg in request.args:
            topics += arg
        return arg, 200
    else:
        return "", 200

# get list of GET'table topics
@app.route("/my/api/value", methods=['GET', 'PUT', 'POST', 'DELETE'])
def dataRouteHandler():
    global topics
    global topic_dict
    if request.method == 'GET':
        for _arg in request.args:
            print "arg: " + _arg
            if _arg in topics:
                print "arg: " + _arg + "x[arg]" + str(topic_dict[_arg])
                return str(topic_dict[_arg]), 200
    elif request.method == 'POST':
        for arg in request.args:
            print "arg: " + arg
            if arg in topics:
                print "arg: " + arg + "x[arg]" + str(topic_dict[arg])
                if arg == 'temp':
                    topic_dict['temp'] = sensors.getTemperature()
                elif arg == 'humid':
                    topic_dict['humid'] = sensors.getHumidity()
                elif arg == 'pressure':
                    topic_dict['pressure'] = sensors.getPressure()
            return str(topic_dict[arg]), 200
    else:
        return "", 200


# todo: write method to get data from sensehat based on tag (temp)

def isPrime2(n):
    if n==2 or n==3: return True
    if n%2==0 or n<2: return False
    for i in range(3,int(n**0.5)+1,2):   # only odd numbers
        if n%i==0:
            return False    
    return True

@app.route("/my/api/prime/<int:num>", methods=['GET','PUT','POST','DELETE'])
def primeRouteHandler(num):
    foo = ""
    if request.method == 'GET':
        if isPrime2(num) == True:
            foo = str(num) + " is prime\r\n"
        else:
            foo = str(num) + " is not prime\r\n"
        return foo, 200
    else:
        return "", 200

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)

