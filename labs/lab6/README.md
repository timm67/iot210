

Here is my thing
````
{
    "things": [
        {
            "thingArn": "arn:aws:iot:us-west-2:062681437620:thing/timThing1Name", 
            "version": 1, 
            "thingName": "timThing1Name", 
            "attributes": {}
        }
    ]
}
```
Here are the certificates associated with the thing:
```
{
    "certificates": [
        {
            "certificateArn": "arn:aws:iot:us-west-2:062681437620:cert/d171cbc65d34bc7bd100c96d5a652ace16935a31ad7bdb922c491a1bcfc3fe48", 
            "status": "ACTIVE", 
            "creationDate": 1519109875.618, 
            "certificateId": "d171cbc65d34bc7bd100c96d5a652ace16935a31ad7bdb922c491a1bcfc3fe48"
        }, 
        {
            "certificateArn": "arn:aws:iot:us-west-2:062681437620:cert/609b1d7233c1f2c8b59013bda4f5aa02b1a172772de65728f22ad278f4e0d9f6", 
            "status": "ACTIVE", 
            "creationDate": 1519109573.418, 
            "certificateId": "609b1d7233c1f2c8b59013bda4f5aa02b1a172772de65728f22ad278f4e0d9f6"
        }, 
        {
            "certificateArn": "arn:aws:iot:us-west-2:062681437620:cert/e4844a0acd509a033263ba6fc36eb381d363ae64287e3ff00eeb991b0d28675c", 
            "status": "ACTIVE", 
            "creationDate": 1519106819.216, 
            "certificateId": "e4844a0acd509a033263ba6fc36eb381d363ae64287e3ff00eeb991b0d28675c"
        }, 
        {
            "certificateArn": "arn:aws:iot:us-west-2:062681437620:cert/d5fa39b6423bab2602636d64b6f093e35388e5ea2372d9120d60ee0375793532", 
            "status": "ACTIVE", 
            "creationDate": 1518148509.193, 
            "certificateId": "d5fa39b6423bab2602636d64b6f093e35388e5ea2372d9120d60ee0375793532"
        }
    ]
}
```
Here are the principals associated with the thing
```
{
    "principals": [
        "arn:aws:iot:us-west-2:062681437620:cert/e4844a0acd509a033263ba6fc36eb381d363ae64287e3ff00eeb991b0d28675c", 
        "arn:aws:iot:us-west-2:062681437620:cert/d171cbc65d34bc7bd100c96d5a652ace16935a31ad7bdb922c491a1bcfc3fe48"
    ]
}
```

and AWS says that the actual endpoint address for the thing is the following:
```
{
    "endpointAddress": "a3bhn8ag9bkj5.iot.us-west-2.amazonaws.com"
}
```

I tried with difficulty to update the shadow parameters of the thing using one of the aws command line utilities. It is possible that 
I did not have the parameters created, but it seemed not to process the inline json properly:

```
timm@timm-iot110:~/iot210/labs/lab6 $ aws iot-data update-thing-shadow --thing-name timThing1Name --payload  { "state" : { "desired" : { "color" : "red", "p
ower" : "on" }}}
Cusage: aws [options] <command> <subcommand> [<subcommand> ...] [parameters]
To see help text, you can run:

  aws help
  aws <command> help
  aws <command> <subcommand> help

Unknown options: {, desired, :, {, color, :, red,, power, :, on, }}}, :
```

I tried a similar way with a HTTPS POST, but the thing was not able to be found by AWS when I used the endpoint address (assuming https)

```
timm@timm-iot110:~/iot210/labs/lab6 $ curl -X POST -H "Content-type: application/json" --data "{ "state" : { "desired" : { "color" : "red", "power" : "on" }}}" --url https://a3bhn8ag9bkj5.iot.us-west-2.amazonaws.com
{"message":"Not Found","traceId":"0a22002c-ca11-a898-2d8c-8fc542354138"}
timm@timm-iot110:~/iot210/labs/lab6 $
```



