As part of the Heroku exercise of this lab, I created a Heroku account and uploaded
the Quizzling application. I tried it on a Windows laptop which proved to be 
not a successful undertaking (and produced immense-scrubland). I did it direct
from the Pi and was successful in uploading the project to Heroku. The URL and 
an operation to get the student quiz token are shown below. 

```
timm@timm-iot110:~/iot210/labs/lab7 $ heroku login
Enter your Heroku credentials:
Email: tim.meese67@gmail.com
Password: *********
Logged in as tim.meese67@gmail.com

timm@timm-iot110:~/iot210/labs/lab7 $ heroku apps
=== tim.meese67@gmail.com Apps
arcane-dawn-74092
immense-scrubland-21737

timm@timm-iot110:~/iot210/labs/lab7 $ curl https://arcane-dawn-74092.herokua
pp.com/api/v1/login/timm?pwd=meese
{"token":"TSBXW29O8wpyA3ChPSx3"}
timm@timm-iot110:~/iot210/labs/lab7 $ 
```

